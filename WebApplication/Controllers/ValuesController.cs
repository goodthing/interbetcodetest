﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class ValuesController : ApiController
    {
        public JsonResult<List<Transaction>> Get(string date)
        {
            DateTime dateFrom = getDate(date);
            string connectionString = "Data Source=DESKTOP-T3PTQIQ\\MSSQLSERVER01;Initial Catalog=Banking;Integrated Security=True";
            SqlConnection cn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("SelectTransactions", cn);
            List<Transaction> transactionList = new List<Transaction>();
            Transaction transaction;

            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                SqlParameter parameter = cmd.Parameters.Add("@TransactionDate",
                    System.Data.SqlDbType.DateTime);
                parameter.Value = dateFrom;

                cn.Open();
                SqlDataReader rd = cmd.ExecuteReader();
              
                while (rd.Read())
                {
                    transaction = new Transaction
                    {
                        AccountNumber =(string) rd["AccountNumber"],
                        BankName = (string)rd["BankName"],
                        BranchCode = (string)rd["BranchCode"],
                        ChequeNumber = (string)rd["ChequeNumber"],
                        DrCrIndicator = (string)rd["DrCrIndicator"],
                        EffectiveDate = (DateTime)rd["EffectiveDate"],
                        Identifier = (string)rd["Identifier"],
                        ReferenceNumber = (string)rd["ReferenceNumber"],
                        TransactionAmount = (decimal)rd["TransactionAmount"],
                        TransactionCode = (string)rd["TransactionCode"],
                        TransactionDate = (DateTime)rd["TransactionDate"],
                        TransactionTime = (string)rd["TransactionTime"],
                        UniqueInstanceID = (string)rd["UniqueInstanceID"],
                    };
                   
                    if (transaction != null)
                    {
                        transactionList.Add(transaction);
                    }
                }
                rd.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"An error occurred: {ex.InnerException.ToString()}");
            }

            var goodReturn = JsonConvert.SerializeObject(transactionList,
                          new JsonSerializerSettings
                          {
                              PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                              ContractResolver = new CamelCasePropertyNamesContractResolver()
                          });

            return Json(transactionList);
        }

        private static DateTime getDate(string dateString)
        {
            DateTime dt;
            DateTime.TryParseExact(dateString,
                                   "yyyy-MM-dd",
                                   CultureInfo.InvariantCulture,
                                   DateTimeStyles.None,
                                   out dt);

            return dt;
        }
    }
}
