﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public string UniqueInstanceID { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string TransactionCode { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string ChequeNumber { get; set; }
        public string DrCrIndicator { get; set; }
        public string BankName { get; set; }
        public string BranchCode { get; set; }
        public string ReferenceNumber { get; set; }
        public string AccountNumber { get; set; }
        public string Identifier { get; set; }
    }
}