﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{
    public class Transaction
    {
        public int Id { get; set; }
        [System.Xml.Serialization.XmlElement("UniqueInstanceID")]
        public string UniqueInstanceID { get; set; }

        [System.Xml.Serialization.XmlElement("EffectiveDate")]
        public DateTime EffectiveDate { get; set; }

        [System.Xml.Serialization.XmlElement("TransactionCode")]
        public string TransactionCode { get; set; }

        [System.Xml.Serialization.XmlElement("TransactionAmount")]
        public decimal TransactionAmount { get; set; }

        [System.Xml.Serialization.XmlElement("TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [System.Xml.Serialization.XmlElement("TransactionTime")]
        public string TransactionTime { get; set; }

        [System.Xml.Serialization.XmlElement("ChequeNumber")]
        public string ChequeNumber { get; set; }

        [System.Xml.Serialization.XmlElement("DrCrIndicator")]
        public string DrCrIndicator { get; set; }

        [System.Xml.Serialization.XmlElement("BankName")]
        public string BankName { get; set; }

        [System.Xml.Serialization.XmlElement("BranchCode")]
        public string BranchCode { get; set; }

        [System.Xml.Serialization.XmlElement("ReferenceNumber")]
        public string ReferenceNumber { get; set; }

        [System.Xml.Serialization.XmlElement("AccountNumber")]
        public string AccountNumber { get; set; }

        [System.Xml.Serialization.XmlElement("Identifier")]
        public string Identifier { get; set; }
    }
}
