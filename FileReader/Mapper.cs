﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileReader
{
    public static class Mapper
    {
        public static Transaction Map(CorporatePayments source)
        {
            return new Transaction
            {
                AccountNumber = source.AccountNumber,
                BankName = source.BankName,
                BranchCode = source.BranchCode,
                ChequeNumber = source.ChequeNumber == "" ? "" : source.ChequeNumber,
                DrCrIndicator = source.DrCrIndicator,
                EffectiveDate = getDate(source.EffectiveDate),
                Identifier = source.Identifier,
                ReferenceNumber = source.ReferenceNumber,
                TransactionAmount = Convert.ToDecimal(source.TransactionAmount),
                TransactionCode = source.TransactionCode,
                TransactionDate = getDate(source.TransactionDate),
                TransactionTime = source.TransactionTime,
                UniqueInstanceID = source.UniqueInstanceID
            };
        }

        private static DateTime getDate(string dateString)
        {
            DateTime dt;
            DateTime.TryParseExact(dateString,
                                   "yyyyMMdd",
                                   CultureInfo.InvariantCulture,
                                   DateTimeStyles.None,
                                   out dt);

            return dt;
        }
    }
}
