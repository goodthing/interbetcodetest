﻿namespace FileReader
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startPoll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.stopPoll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startPoll
            // 
            this.startPoll.Location = new System.Drawing.Point(61, 39);
            this.startPoll.Name = "startPoll";
            this.startPoll.Size = new System.Drawing.Size(75, 23);
            this.startPoll.TabIndex = 0;
            this.startPoll.Text = "Start Polling";
            this.startPoll.UseVisualStyleBackColor = true;
            this.startPoll.Click += new System.EventHandler(this.startPoll_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ready";
            // 
            // stopPoll
            // 
            this.stopPoll.Location = new System.Drawing.Point(61, 68);
            this.stopPoll.Name = "stopPoll";
            this.stopPoll.Size = new System.Drawing.Size(75, 23);
            this.stopPoll.TabIndex = 2;
            this.stopPoll.Text = "Stop Polling";
            this.stopPoll.UseVisualStyleBackColor = true;
            this.stopPoll.Click += new System.EventHandler(this.stopPoll_Click);
            // 
            // MainForm
            // 
            this.ClientSize = new System.Drawing.Size(523, 261);
            this.Controls.Add(this.stopPoll);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.startPoll);
            this.Name = "MainForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button startPoll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button stopPoll;
    }
}

