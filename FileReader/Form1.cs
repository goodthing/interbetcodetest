﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using System.Xml.Serialization;
using System.Xml;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Globalization;

namespace FileReader
{
    public partial class MainForm : Form
    {
        private string dropFilePath =
           ConfigurationManager.AppSettings["dropFilePath"];
        private int pollInterval =
            Int32.Parse(ConfigurationManager.AppSettings["pollInterval"]);
        private static System.Timers.Timer timer = new System.Timers.Timer();

        public MainForm()
        {
            InitializeComponent();
        }

        private void startPoll_Click(object sender, EventArgs e)
        {
            timer.Interval = pollInterval;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            System.Threading.Thread.Sleep(1000);
            timer.Start();
            label1.Text = "Now polling";
        }

        private void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            Directory.EnumerateFiles(dropFilePath, "*.txt", SearchOption.AllDirectories)
                .ToList()
                .ForEach(f => PersistTransaction(f));
        }

        private void PersistTransaction(string filename)
        {
            CorporatePayments payment;
            Transaction transaction;
            XmlSerializer serializer = new XmlSerializer(typeof(CorporatePayments));
            StreamReader reader;
            string connectionString = "Data Source=DESKTOP-T3PTQIQ\\MSSQLSERVER01;Initial Catalog=Banking;Integrated Security=True";
            SqlConnection cn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("InsertTransaction", cn);

            try
            {
                reader = new StreamReader(filename);
                payment = (CorporatePayments)serializer.Deserialize(reader);
                transaction = Mapper.Map(payment);
                reader.Close();

                cn.Open();

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@AccountNumber", transaction.AccountNumber);
                cmd.Parameters.AddWithValue("@BankName", transaction.BankName);
                cmd.Parameters.AddWithValue("@BranchCode", transaction.BranchCode);
                cmd.Parameters.AddWithValue("@ChequeNumber", transaction.ChequeNumber);
                cmd.Parameters.AddWithValue("@DrCrIndicator", transaction.DrCrIndicator);
                cmd.Parameters.AddWithValue("@EffectiveDate", transaction.EffectiveDate);
                cmd.Parameters.AddWithValue("@Identifier", transaction.Identifier);
                cmd.Parameters.AddWithValue("@ReferenceNumber", transaction.ReferenceNumber);
                cmd.Parameters.AddWithValue("@TransactionAmount", transaction.TransactionAmount);
                cmd.Parameters.AddWithValue("@TransactionCode", transaction.TransactionCode);
                cmd.Parameters.AddWithValue("@TransactionDate", transaction.TransactionDate);
                cmd.Parameters.AddWithValue("@TransactionTime", transaction.TransactionTime);
                cmd.Parameters.AddWithValue("@UniqueInstanceID", transaction.UniqueInstanceID);
                cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"An error occurred: {ex.InnerException.ToString()}");
            }

            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }

        private void stopPoll_Click(object sender, EventArgs e)
        {
            timer.Stop();
            label1.Text = "Ready";
        }
    }

   
}
